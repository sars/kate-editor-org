# kate-editor.org

This repository contains the full kate-editor.org website.

On push the https://binary-factory.kde.org/job/Website_kate-editor-org/ job will trigger an update of the web server.

# Live preview

This repo uses a theme shared with other Hugo repos.  
For local development, by default, the shared theme is expected to be at `../aether-sass`, relative to the root of this repo. You can clone it via
```bash
git clone git@invent.kde.org:websites/aether-sass.git -b hugo --single-branch
```
You can also use the theme you have at another location by changing the path in the development config file `config/development/config.yaml`.  
If you don't want to touch the shared theme, simply delete the development config file and uncomment the `hugo mod get` line in `server.sh`.

Then you can start a local hugo powered web server via

```bash
./server.sh
```

The command will print the URL to use for local previewing.

# Update the syntax-highlighting framework update sites

Check out the README.md in the syntax-highlighting.git on invent.kde.org.

There is a build target to generate the needed stuff after a successful compile of the framework.

# Update auto-generated pages like "team" or "merge requests"

Just run

```bash
./regenerate.sh
```

and review the results before you commit them.
