#!/bin/bash

# failures are evil
set -e

# update team statistics
./the-team-update.pl

# update merge request statistics
./merge-requests-update.py
