# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-License-Identifier: LGPL-2.0-or-later

import frontmatter
import os
import subprocess
import argparse
import gettext
import copy
import polib
import icu
import typing as tp
import re


from yaml import safe_load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

def webpage_list() -> tp.Dict[str, tp.List[str]]:
    """
    Generate a list of webpage that need to be translated
    """
    return ['content/about-kate.en.md', 'content/_index.en.md', 'content/get-it.en.md']


excluded_keys = ['layout', 'date', 'type', 'publishDate', 'version', 'release', 'qtversion', 'changelog', 'sassFiles', 'link', 'image', 'externi18n', 'draft', 'color', 'sassFile', 'aliases', 'laptop', 'images', 'class', 'logo', 'src', 'libCount', 'weight', 'parent', 'hideMeta']


def import_frontmatter(data, _):
    """
    Import translation from po files. This function is recursive and support
    list and dict.
    """
    has_translations = False
    if isinstance(data, list):
        for index, item in enumerate(data):
            if isinstance(item, str):
                if item != _(item):
                    has_translations = True
                    data[index] = _(item)
            else:
                has_translations = import_frontmatter(item, _) or has_translations
    elif isinstance(data, dict):
        for key in data:
            if key in excluded_keys:
                continue

            if isinstance(data[key], str):
                if data[key] != _(data[key]):
                    has_translations = True
                    data[key] = _(data[key])
            else:
                has_translations = import_frontmatter(data[key], _) or has_translations

    return has_translations


def extract_frontmatter(data, pot, filename):
    """
    Export translation to po files. This function is recursive and support
    list and dict.
    """
    if isinstance(data, str):
        entry = polib.POEntry(
            msgid=data,
            msgstr=u'',
            occurrences=[(filename, '0')]
        )
        try:
            pot.append(entry)
        except Exception:
            pass
    elif isinstance(data, list):
        for item in data:
            extract_frontmatter(item, pot, filename)
    elif isinstance(data, dict):
        for key in data:
            if key not in excluded_keys:
                extract_frontmatter(data[key], pot, filename)


def process_line(line: str) -> tp.Tuple[tp.List[str], str, str]:
    """
    Process a line. Give a tuple containing the extracted string, a comment
    and a format string to recreate the old line.
    """
    line = line.rstrip()
    if line.startswith('#'):
        heading_level = len(line) - len(line.lstrip('#'))
        if line[heading_level] != ' ':
            print('WARNING: naughty heading we have here!')
        line = line[heading_level + 1:]
        if line.startswith('{{< i18n_var ') or line.startswith('{{% i18n_var '):
            prog = re.compile('i18n_var "([^"\\\\]*(?:\\.[^"\\\\]*)*)"')
            result = prog.findall(line)
            if result:
                return [result[0]], 'type: List item', re.sub('i18n_var "([^"\\\\]*(?:\\.[^"\\\\]*)*)"', 'i18n_var "{}"', re.sub('{', '{{', re.sub('}', '}}', line)))

            prog = re.compile("i18n_var `([^']*)`")
            result = prog.findall(line)
            if result:
                return [result[0]], 'type: html text', re.sub("i18n_var `([^`]*)`", "i18n_var `{}`", re.sub('{', '{{', re.sub('}', '}}', line)))
        return [line], 'type: Title ' + (heading_level * '#'), '#' * heading_level + ' {}'

    if len(line) > 2 and line[0] in ('+', '-', '*') and line[1] == ' ':
        line_content = line[2:]
        if line_content.startswith('{{< i18n_var ') or line_content.startswith('{{% i18n_var '):
            prog = re.compile('i18n_var "([^"\\\\]*(?:\\.[^"\\\\]*)*)"')
            result = prog.findall(line_content)
            if result:
                return [result[0]], 'type: List item', re.sub('i18n_var "([^"\\\\]*(?:\\.[^"\\\\]*)*)"', 'i18n_var "{}"', re.sub('{', '{{', re.sub('}', '}}', line_content)))

            prog = re.compile("i18n_var `([^']*)`")
            result = prog.findall(line_content)
            if result:
                return [result[0]], 'type: html text', re.sub("i18n_var `([^`]*)`", "i18n_var `{}`", re.sub('{', '{{', re.sub('}', '}}', line_content)))
        return [line_content], 'type: List item', line[0] + ' {}'

    if 'i18n_var' in line:
        prog = re.compile('i18n_var "([^"\\\\]*(?:\\.[^"\\\\]*)*)"')
        result = prog.findall(line)
        if result:
            return [result[0]], 'type: html text', re.sub('i18n_var "([^"\\\\]*(?:\\.[^"\\\\]*)*)"', 'i18n_var "{}"', re.sub('{', '{{', re.sub('}', '}}', line)))

        prog = re.compile("i18n_var `([^']*)`")
        result = prog.findall(line)
        if result:
            return [result[0]], 'type: html text', re.sub("i18n_var `([^`]*)`", "i18n_var `{}`", re.sub('{', '{{', re.sub('}', '}}', line)))

    if line.startswith('{{<') or line.startswith('{{%'):
        prog = re.compile('(caption|description|alt|title)="([^"\\\\]*(?:\\.[^"\\\\]*)*)"')
        results = prog.findall(line)
        r = []
        for result in results:
            r.append(result[1])
        return r, 'type: shortcode parameter', re.sub('(caption|description|alt|title)="([^"\\\\]*(?:\\.[^"\\\\]*)*)"', r'\1=`{}`', re.sub('{', '{{', re.sub('}', '}}', line)))

    return [line], 'type: Plain Text', '{}'


def add_entry_to_pot(pot, entry, comment, filename, line_number):
    if entry != "":
        old_entry = pot.find(entry)
        if old_entry:
            old_entry.occurrences.append((filename, line_number))
        else:
            po_entry = polib.POEntry(
                msgid=entry,
                msgstr=u'',
                occurrences=[(filename, line_number)],
                comment=comment
            )
            try:
                pot.append(po_entry)
            except Exception:
                pass


def extract_content(post, pot, filename: str):
    """
    Extract main content of a page.

    Seperate it into multiples logicial string (e.g. split strings by
    paragraphs, list items, ...).
    """
    sectionContent = ""
    parsing_list_element = False
    line_number = 4 # approximation
    for line in post.content.splitlines():
        if 'stop-translate' in line:
            break
        line = ' '.join((line + " ").split())
        if len(line) == 0 or (parsing_list_element and (line[0] in ('+', '-', '*') and line[1] == ' ')):
            #print('"' + sectionContent +'"')
            strings = process_line(sectionContent)
            for string in strings[0]:
                add_entry_to_pot(pot, string, strings[1], filename, line_number)
            sectionContent = ""
            parsing_list_element = False
            if line:
                sectionContent += line
        else:
            sectionContent += line + " "

        if len(line) > 2 and line[0] in ('+', '-', '*') and line[1] == ' ':
            parsing_list_element = True
        line_number += 1

    strings = process_line(sectionContent)
    for string in strings[0]:
        add_entry_to_pot(pot, string, strings[1], filename, line_number)


def import_content(post, _) -> tp.Tuple[str, float]:
    """
    Import translation for main content.

    Similar to export_content in the way it works.
    """
    sectionContent = ""
    parsing_list_element = False
    translated = ""
    totalTranslated = 0
    totalString = 0
    for line in post.content.splitlines():
        if 'stop-translate' in line:
            break
        line = ' '.join((line + " ").split())
        list_item = len(line) > 2 and line[0] in ('+', '-', '*') and line[1] == ' '
        if len(line) == 0 or (parsing_list_element and list_item):
            processed_line = process_line(sectionContent)
            if not processed_line:
                translated += sectionContent + '\n\n'
            else:
                is_translated = len(processed_line[0]) > 0
                for string in processed_line[0]:
                    if _(string) == string or len(_(string)) == 0 or len(string) == 0:
                        is_translated = False

                if not is_translated:
                    translated += sectionContent + ('\n' if list_item else '\n\n')
                else:
                    try:
                        translated += processed_line[2].format(*tuple([_(x).replace('\r', ' ').replace('\n', ' ') for x in processed_line[0]])) + ('\n' if list_item else '\n\n')
                    except:
                        translated += sectionContent + ('\n' if list_item else '\n\n')
                        print("Bug in '{}'".format(sectionContent))

                    totalTranslated += 1

                totalString += 1

            sectionContent = ""
            parsing_list_element = False
            if len(line) != 0:
                sectionContent += line
        else:
            sectionContent += line + " "

        if len(line) > 2 and line[0] in ('+', '-', '*') and line[1] == ' ':
            parsing_list_element = True

    list_item = len(sectionContent) > 2 and sectionContent[0] in ('+', '-', '*') and sectionContent[1] == ' '
    processed_line = process_line(sectionContent)
    is_translated = len(processed_line[0]) > 0
    for string in processed_line[0]:
        if _(string) == string or len(_(string)) == 0 or len(string) == 0:
            is_translated = False
    if not is_translated:
        translated += sectionContent + '\n\n'
    else:
        translated += processed_line[2].format(*tuple([_(x).replace('\r', ' ').replace('\n', ' ') for x in processed_line[0]])) + ('\n' if list_item else '\n\n')
        totalTranslated += 1

    totalString += 1

    return translated, totalTranslated / totalString


def extract(args):
    """
    First parameter will be the path of the pot file we have to create
    """
    pot_file = args.pot
    pot = polib.POFile(check_for_duplicates=True)
    pot.metadata = {
        'Project-Id-Version': '1.0',
        'Report-Msgid-Bugs-To': 'kwrite-devel@kde.org',
        'Last-Translator': 'you <you@example.com>',
        'Language-Team': 'English <yourteam@example.com>',
        'MIME-Version': '1.0',
        'Content-Type': 'text/plain; charset=utf-8',
        'Content-Transfer-Encoding': '8bit',
    }
    for webpage in webpage_list():
        post = frontmatter.load(webpage)

        extract_frontmatter(post.metadata, pot, webpage)
        extract_content(post, pot, webpage)

    entry = polib.POEntry(
        msgid=u'LANGUAGE_NAME',
        msgstr=u'',
        comment=u'Replace LANGUAGE_NAME with the name of your language in your language (e.g. Français, Deutsch, Українська ...). Displayed in the navbar',
        occurrences=[('translations.py', '223')]
    )
    try:
        pot.append(entry)
    except:
        pass

    with open("i18n/en.yaml", 'r') as stream:
        en_string_trans = safe_load(stream)

        for key, string in en_string_trans.items():
            entry = polib.POEntry(
                msgid=string['other'],
                msgstr=u'',
                occurrences=[('i18n/en.yaml', '0')]
            )
            try:
                pot.append(entry)
            except:
                pass

    with open("config.yaml", "r") as stream:
        config = safe_load(stream)

        for menu_item in config['languages']['en']['menu']['main']:
            entry = polib.POEntry(
                msgid=menu_item['name'],
                msgstr=u'',
                occurrences=[('config.yaml', '0')]
            )
            try:
                pot.append(entry)
            except:
                pass

        entry = polib.POEntry(
            msgid=config['languages']['en']['title'],
            msgstr=u'',
            occurrences=[('config.yaml', '0')]
        )
        try:
            pot.append(entry)
        except:
            pass

    pot.save(pot_file)


def import_po(args):
    """
    First parameter will be a path that will contain several .po files with the format LANG.po
    """
    directory = args.directory

    for translation in os.listdir(directory):
        lang = os.path.splitext(translation)[0]

        target_path = "locale/" + lang + "/LC_MESSAGES"
        os.makedirs(target_path)
        src = directory + "/" + translation

        command = "msgfmt {} -o {}/kate.mo".format(src, target_path)
        subprocess.run(command, shell=True, check=True)
        print("Translations for " + lang + " imported")


lang_fixer = re.compile(r'[@_]')
def convert_lang_code(lang_code):
    if lang_code == 'pt':
        return 'pt-pt'
    else:
        hugo_lang_code = lang_fixer.sub('-', lang_code.lower())
        parts = hugo_lang_code.split('-')
        if len(parts) > 1:
            if parts[1] == 'ijekavian':
                hugo_lang_code = parts[0] + '-ije'
            elif parts[1] == 'ijekavianlatin':
                hugo_lang_code = parts[0] + '-il'
            elif len(parts[1]) > 2:
                hugo_lang_code = parts[0] + '-' + parts[1][0:2]
        return hugo_lang_code


def generate_translations(args):
    """
    Assume translation located at `locale/$LANG/LC_MESSAGES/`
    """
    with open("i18n/en.yaml", 'r') as stream:
        en_string_trans = safe_load(stream)
    with open("config.yaml", 'r') as config_file:
        config_content = safe_load(config_file)

    for lang_code in os.listdir('locale'):
        os.environ["LANGUAGE"] = lang_code
        translations = convert_lang_code(lang_code)

        gettext.bindtextdomain('kate', 'locale')
        gettext.textdomain('kate')
        _ = gettext.gettext

        for webpage in webpage_list():
            extension = os.path.splitext(webpage)[1]
            basename = os.path.splitext(webpage)[0].split('.')[0]
            translated = basename + "." + translations + extension

            post = frontmatter.load(webpage)
            has_translations = import_frontmatter(post.metadata, _)
            content, percent = import_content(post, _)

            if percent > 0.5 or has_translations:
                with open(translated, 'w+') as translation_file:
                    translation_file.write('---\n')
                    translation_file.write(dump(post.metadata, default_flow_style=False, allow_unicode=True))
                    translation_file.write('---\n')
                    translation_file.write(content)

                with open(translated) as f_input:
                    data = f_input.read().rstrip('\n')
                with open(translated, 'w') as f_output:
                    f_output.write(data)

        trans_content = dict()
        for key, string in en_string_trans.items():
            if _(string['other']) is not string['other']:
                trans_content[key] = dict()
                trans_content[key]['other'] = _(string['other'])

        if len(trans_content) > 0:
            with open('i18n/' + translations + '.yaml', 'w+') as trans_file:
                trans_file.write(dump(trans_content, default_flow_style=False, allow_unicode=True))

            if not translations in config_content['languages']:
                config_content['languages'][translations] = dict()

            config_content['languages'][translations]['menu'] = dict()
            config_content['languages'][translations]['menu']['main'] = list()
            config_content['languages'][translations]['languageCode'] = translations
            config_content['languages'][translations]['title'] = _(config_content['languages']['en']['title'])
            config_content['languages'][translations]['weight'] = 2

            tr = _("LANGUAGE_NAME")
            if tr != "LANGUAGE_NAME":
                config_content['languages'][translations]['languageName'] = tr
            else:
                # Fallback when translators didn't include any LANGUAGE_NAME
                # So that we don't have an empty menu entry (or less of them).
                locale = icu.Locale(translations)
                try:
                    config_content['languages'][translations]['languageName'] = locale.getDisplayName(locale).capitalize()
                except icu.InvalidArgsError:
                    print("error {}".format(translations))

            for menu_item in config_content['languages']['en']['menu']['main']:
                menu = copy.deepcopy(menu_item)
                menu['name'] = _(menu['name'])
                config_content['languages'][translations]['menu']['main'].append(menu)
        elif translations in config_content['languages']:
            del config_content['languages'][translations]

    with open('config.yaml', 'w+') as conf_file:
        conf_file.write(dump(config_content, default_flow_style=False, allow_unicode=True))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='sub-command help')

    extract_cmd = subparsers.add_parser('extract', help='extract strings for translations')
    extract_cmd.add_argument('pot')
    extract_cmd.set_defaults(func=extract)

    import_po_cmd = subparsers.add_parser('import', help='import translated strings')
    import_po_cmd.add_argument('directory')
    import_po_cmd.set_defaults(func=import_po)

    generate_translations_cmd = subparsers.add_parser('generate-translations', help='generate translated content')
    generate_translations_cmd.set_defaults(func=generate_translations)

    args = parser.parse_args()
    args.func(args)
