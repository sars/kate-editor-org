---
layout: index
---
Kate jest wielodokumentowym edytorem [KDE](https://kde.org) od wydania 2.2. Będąc [Aplikacją KDE](https://kde.org/applications), Kate jest dostarczane na zasadzie przezroczystości sieci, a także z wbudowanymi możliwościami jakie daje KDE. Wybierz go do oglądania źródeł HTML z konquerora, zmieniania plików ustawień, pisania nowych aplikacji lub innych zadań edycji tekstu. Nadal wystarczy tylko jedno wystąpienie Kate. [Dowiedz się wiecej...](/about/)

![Zrzut ekranu Kate pokazujący wiele dokumentów i emulator terminala podczas
budowy ](/images/kate-window.png)