---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
discoverData:
  kate:
    alt: Instal·lau el Kate a través del Discover o altres botigues d'aplicacions
      AppStream
    subtitle: A través del Discover
    title: Instal·leu el Kate
  kwrite:
    alt: Instal·lau el KWrite a través del Discover o altres botigues d'aplicacions
      AppStream
    subtitle: A través del Discover
    title: Instal·leu el KWrite
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /sass/get-it.scss
title: Obteniu el Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}} 

### Linux i Unixs

+ Instal·leu el [Kate](https://apps.kde.org/ca/kate) o el [KWrite](https://apps.kde.org/ca/kwrite) des de la [vostra distribució](https://www.kde.org/ca/distributions/)

{{< get-it-button-discover >}} 

Aquests botons només funcionen amb el [Discover](https://apps.kde.org/ca/discover) o amb altres botigues d'aplicacions AppStream, com la [GNOME Software](https://wiki.gnome.org/Apps/Software). També podeu usar el gestor de paquets de la vostra distribució, com el [Muon](https://apps.kde.org/ca/muon) o el [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}} 

+ El [paquet Snap del Kate a Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-badge-en.png" width="200" alt=`Obteniu el Kate des de la botiga Snap` />}}

+ [Paquet AppImage de llançament del Kate (64bits)](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [Paquet AppImage nocturn del Kate (64bits)](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **
+ [Construïu-lo](/build-it/#linux) a partir del codi font.

{{< /get-it >}} 

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}} 

### Windows 

+ El [Kate a la Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.png" width="200" alt=`Obteniu el Kate des de la botiga Microsoft` />}}

+ El [Kate via Chocolatey](https://chocolatey.org/packages/kate)
+ L'[instal·lador del llançament del Kate (64bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ L'[instal·lador de la versió nocturna del Kate (64bits)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Construïu-lo](/build-it/#windows) a partir del codi font.

{{< /get-it >}} 

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}} 

### macOS 

+ L'[instal·lador del llançament del Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ L'[instal·lador de la versió nocturna del Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Construïu-lo](/build-it/#mac) a partir del codi font.

{{< /get-it >}} 



{{< get-it-info >}} 

**Quant als llançaments:** <br>El [Kate](https://apps.kde.org/ca/kate) i el [KWrite](https://apps.kde.org/ca/kwrite) són part de les [aplicacions KDE](https://apps.kde.org/ca), que es [publiquen normalment 3 vegades a l'any de forma completa](https://community.kde.org/Schedules). El [motor d'edició de text](https://api.kde.org/frameworks/ktexteditor/html/) i el [ressaltat de sintaxi](https://api.kde.org/frameworks/syntax-highlighting/html/) estan proporcionats pels [Frameworks de KDE](https://kde.org/ca/announcements/kde-frameworks-5.0/), que s'[actualitzen mensualment](https://community.kde.org/Schedules/Frameworks). Els llançaments nous s'anuncien [ací](https://kde.org/ca/announcements/).

\* Els paquets del **llançament** contenen la última versió del Kate i dels Frameworks de KDE.

\*\* Els paquets **nocturns** es compilen automàticament cada dia a partir del codi font, i per tant, poden ser inestables i poden haver-hi errors o funcionalitats incompletes. Només són recomanats amb la finalitat de fer proves.

{{< /get-it-info >}} 