---
title: Animated Bracket Matching in Kate Part
author: Dominik Haumann

date: 2013-11-06T15:20:43+00:00
url: /2013/11/06/animated-bracket-matching-in-kate-part/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users

---
Kate in 4.13 will have a new features: **Animated bracket matching!**

<p style="text-align: center;">
  <img class="alignnone size-full wp-image-2924" title="Animated Bracket Matching: Light color scheme" src="/wp-content/uploads/2013/11/lightcs.gif" alt="" width="350" height="100" srcset="/wp-content/uploads/2013/11/lightcs.gif 350w, /wp-content/uploads/2013/11/lightcs-300x85.gif 300w" sizes="(max-width: 350px) 100vw, 350px" /><br /> <img class="alignnone size-full wp-image-2925" title="Animated Bracket Matching: Dark color scheme" src="/wp-content/uploads/2013/11/darkcs.gif" alt="" width="350" height="100" srcset="/wp-content/uploads/2013/11/darkcs.gif 350w, /wp-content/uploads/2013/11/darkcs-300x85.gif 300w" sizes="(max-width: 350px) 100vw, 350px" />
</p>

Since the feature might be visually distracting, it is turned off by default. To enable this feature, you have to go into the &#8220;Appearance&#8221; config page and check &#8220;[x] Animate bracket matching.&#8221; Due to feature and message freeze, this feature will be available in 4.13 and not in 4.12.

By the way, over the years we were asked several times to add the feature to jump to the matching bracket. This feature already exists. It is called &#8220;Move to Matching Bracket&#8221; and is bound by default to the shortcut &#8220;Ctrl+6&#8221;. You can change the shortcut in the &#8220;Configure Shortcuts&#8221; dialog.