---
title: Kate on KDE Frameworks 5
author: Christoph Cullmann

date: 2013-12-08T17:03:01+00:00
url: /2013/12/08/kate-on-kde-frameworks-5/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
After the initial porting of KTextEditor interfaces and KatePart, now the Kate application itself runs on KF5, too.  
It still has a LOT of issues (and I marked all commented out pieces with FIXME KF5), but at least, it launches and loads the essential file tree plugin and allows you to open files via the &#8220;Open&#8221; action. Any help in fixing the remaining issues is welcome (and removing deprecated cruft), but keep in mind, it might eat all the files you edit :)  
[<img class="aligncenter size-full wp-image-3044" title="Kate on KF5" src="/wp-content/uploads/2013/12/kate_on_kf5.png" alt="" width="762" height="527" srcset="/wp-content/uploads/2013/12/kate_on_kf5.png 762w, /wp-content/uploads/2013/12/kate_on_kf5-300x207.png 300w" sizes="(max-width: 762px) 100vw, 762px" />][1]

That means now all stuff in the &#8220;frameworks&#8221; branch of kate.git beside the &#8220;addons&#8221; directory containing plugins/plasma stuff at least compiles and launches with KF5.

Now the question is how we handle further development. Shall we switch to &#8220;frameworks&#8221; for all major stuff in interfaces/part/application and only let feature work be done in the &#8220;master&#8221; branch for addons (which we then can port later on)?

Feedback is welcome ;)

 [1]: /wp-content/uploads/2013/12/kate_on_kf5.png