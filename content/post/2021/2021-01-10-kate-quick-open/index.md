---
title: Kate Text Editor - Quick Open
author: Christoph Cullmann
date: 2021-01-10T16:15:00+02:00
url: /post/2021/2021-01-10-kate-quick-open/
---

After my [summary of Kate improvements in 2020](/post/2020/2020-12-31-kate-in-2020/), it is my pleasure to show a first improvement coming in 2021.

## Quick Open?

Kate has since years a feature called "quick open".
You reach it via the button at the right of the tabs or the default shortcut "Ctrl-Alt-O".
Quick open will provide a list of the current open documents (and in addition files of the current/all active projects) for quick navigation between them.
Other editors call this often "Go to file..." or similar.

## Quick Open in Kate 20.12

The overall look'n' feel of quick open didn't change since years.
You get some list of the above mentioned documents/files and some wildcard filter at top.

<p align="center">
    <a href="/post/2021/2021-01-10-kate-quick-open/images/kate-20-12-quick-open.png" target="_blank"><img width=700 src="/post/2021/2021-01-10-kate-quick-open/images/kate-20-12-quick-open.png"/></a>
</p>

This does work and allows quick switching between files, but other editors went further than what we had in the past years.

## Quick Open in Kate 21.04

[Waqar Ahmed](https://invent.kde.org/waqar) stepped up with ideas and patches to improve the current state.

There are two major changes in the repository now that will end up in the 21.04 release.

### Fuzzy matching

Instead of wildcard matching, quick open (and other filters inside Kate) will use a fuzzy matching algorithm.

This means, if you e.g. type "kte", this will not just find for example "ktexteditor.cpp" but also stuff like "katemain.cpp", just with the first one being higher up in the results presented as the match is "better".

For details of the implementation refer to this [merge request](https://invent.kde.org/utilities/kate/-/merge_requests/140) and the blog about the [fuzzy matching algorithm](https://www.forrestthewoods.com/blog/reverse_engineering_sublime_texts_fuzzy_match/) we added.

The [original author Forrest Smith allowed to add this as LGPLv2+ licensed code](https://github.com/forrestthewoods/lib_fts/issues/29), if further parts of KDE want to use this, one might move this code to e.g. KStringHandler or similar.

At the moment, we start to in-cooperate this in more filters inside the Kate code base.

### Results representation

The quick open results representation wasn't altered in years.

Waqar now provided [a new implementation](https://invent.kde.org/utilities/kate/-/merge_requests/169) that shows the results in a more concise way.

The mime-type, file name & file path are shown in a compact way and we no longer span the full width of the editor window to improve the layout for people with rather large windows.

<p align="center">
    <a href="/post/2021/2021-01-10-kate-quick-open/images/kate-21-04-quick-open.png" target="_blank"><img width=700 src="/post/2021/2021-01-10-kate-quick-open/images/kate-21-04-quick-open.png"/></a>
</p>

And yes, this is very similar to both the representation the "tab switcher" plugin in Kate provides and the [Visual Studio Code](https://code.visualstudio.com/) "Go to file..." dialog.

## Summary

Quick open will be a lot more usable and many of the filters in Kate will provide better matching thanks to the fuzzy matching algorithm we will use in the 21.04 release.

Thanks to Waqar and all others that provided feedback in the related merge requests & the original writer of the fuzzy matching code we use, Forrest Smith.

More things are in development, see [the current open merge requests](https://invent.kde.org/utilities/kate/-/merge_requests) for Kate.
Any help is welcome!
