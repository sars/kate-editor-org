---
title: 'Kate Internals: The Undo/Redo System'
author: Dominik Haumann

date: 2008-11-14T16:10:00+00:00
url: /2008/11/14/kate-internals-the-undoredo-system/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2008/11/kate-internals-undoredo-system.html
categories:
  - Developers

---
The Kate Editor Component (also called KatePart) has its own <span style="font-weight: bold;">undo/redo system</span>. It did not change much since KDE2 and basically it is very simple. Meanwhile there are classes for [undo/redo support in Qt][1] as well. In fact both systems are very similar. This article focuses on Kate Part&#8217;s system.

<span style="font-weight: bold; font-size: 130%;">Text Operations</span>

First we have to take a look at what actions need to be saved. In Kate Part this basically comes down to

  * insert text or line
  * remove text or line
  * selection changes
  * (and a few others like wrapping a line)

When typing text, each keystroke inserts a character. This is exactly one <span style="font-weight: bold;">undo/redo item</span>. As example, typing a character &#8216;x&#8217; creates a new undo item:

  * the content is &#8216;x&#8217;
  * the type is &#8216;<span style="font-style: italic;">insert text</span>&#8216;

Undo in this case means &#8216;<span style="font-style: italic;">remove x</span>&#8216;. Redo means &#8216;<span style="font-style: italic;">insert x (again)</span>&#8216;. Hence, the undo/redo history is just a list (more like a stack to be precise) of simple edit actions.

<span style="font-size: 130%;"><span style="font-weight: bold;">KateUndo Items</span></span>

In KatePart, each edit primitive in the undo history is based on the class <a title="kateundo.cpp" href="http://websvn.kde.org/trunk/KDE/kdelibs/kate/undo/kateundo.cpp" target="_blank">KateUndo</a>:

<pre>class KateUndo {
public:
  KateUndo(KateDocument* document);
  virtual ~KateUndo();

  enum UndoType {
    editInsertText,
    editRemoveText,
    editWrapLine,
    editUnWrapLine,
    editInsertLine,
    editRemoveLine,
    editMarkLineAutoWrapped,
    editInvalid
  }; 

  virtual bool mergeWith(const KateUndo* undo);

  virtual void undo() = 0;
  virtual void redo() = 0;

  virtual KateUndo::UndoType type() const = 0;
};
</pre>

For each of the edit primitives above exists a subclass of KateUndo, for instance the class for inserting text looks like this:

<pre>class KateEditInsertTextUndo : public KateUndo
{
  public:
    KateEditInsertTextUndo (KateDocument *document,
                            int line, int col, const QString &text);

    virtual void undo();
    virtual void redo();

    bool mergeWith (const KateUndo *undo);

    KateUndo::UndoType type() const { return KateUndo::editInsertText; }

  private:
    const int m_line;
    const int m_col;
    QString m_text;
};
</pre>

<span style="font-size: 130%;"><span style="font-weight: bold;">Item Merging</span></span>

Note the function KateUndo::mergeWith(const KateUndo* undo); This functions merges two undo items of the same type if possible. For instance, typing &#8216;hello world&#8217; inserts one undo item for every character, i.e. 11 undo items of type &#8216;insert text&#8217;. Kate merges those 11 items into only 1 item with the string &#8216;hello world&#8217;. Merging leads to less KateUndo items (less memory) and faster undo/redo replaying.

<span style="font-size: 130%;"><span style="font-weight: bold;">Item Grouping</span></span>

What&#8217;s still missing is the possibility to <span style="font-weight: bold;">group several undo items together</span>. Imagine you have selected the text &#8216;hello world&#8217; and paste the text &#8216;cheers&#8217; from the clipboard. What happens is this

  1. remove selected text
  2. insert text from clipboard

So there are two undo items of different type. They cannot be merged into only one KateUndo item. Though, we want to support undoing both items in one go, that&#8217;s why we add several undo items into undo groups. In KatePart, this is done by the class <a title="KateUndoGroup" href="http://websvn.kde.org/trunk/KDE/kdelibs/kate/undo/kateundo.h?view=markup" target="_blank">KateUndoGroup</a>:

<pre>class KateUndoGroup {
public:
  explicit KateUndoGroup (KateUndoManager *manager,
                          const KTextEditor::Cursor &cursorPosition,
                          const KTextEditor::Range &selectionRange);

  void undo(KTextEditor::View *view);
  void redo(KTextEditor::View *view);

  enum UndoType { ... };

  void addItem (KateUndoGroup::UndoType type, uint line,
                uint col, uint len, const QString &text);
  void setUndoSelection (const KTextEditor::Range &selection);
  void setRedoSelection (const KTextEditor::Range &selection);
  void setUndoCursor(const KTextEditor::Cursor &cursor);
  void setRedoCursor(const KTextEditor::Cursor &cursor);
  bool merge(KateUndoGroup* newGroup,bool complex);
  void safePoint (bool safePoint=true);
};</pre>

Every KateUndo item belongs to one KateUndoGroup. A KateUndoGroup can have an arbitrary count of KateUndo items. In the example above we want to group &#8216;<span style="font-style: italic;">remove selected text</span>&#8216; and &#8216;<span style="font-style: italic;">insert text</span>&#8216; together. Grouping can be explicitely done in the code as follows (simplified version):

<pre>void <a style="font-family: courier new;" href="http://api.kde.org/4.x-api/kdelibs-apidocs/kate/html/katedocument_8cpp-source.html#l04218">KateDocument::paste</a>( KateView* view, QClipboard::Mode mode )
{
  QString s = QApplication::clipboard()-&gt;text(mode);
  editStart();
  view-&gt;removeSelectedText();
  insertText(pos, s, view-&gt;blockSelectionMode());
  editEnd();
}</pre>

<span style="font-size: 130%;"><span style="font-weight: bold;">Grouping: editStart()/editEnd()</span></span>

The call of [editStart()][2] tells the document that an edit operation is running. All text operations are added to the current KateUndoGroup, until [editEnd()][3] is called. editStart() and editEnd() do reference counting, i.e. editStart() can be called nested as long as for each call of editStart() there is (finally) a call of editEnd().

<span style="font-size: 130%;"><span style="font-weight: bold;">Grouping: Cursors and Selections</span></span>

Undoing the paste-action above should restore the selection if there was one previously. Redo (i.e. paste again) should remove the selection again. So there are <span style="font-weight: bold;">two different types of selections</span>: one before the undo group, and one after. That&#8217;s why each undo group has the functions setUndoSelection() and setRedoSelection(). The same applies for the cursor position: We have to store <span style="font-weight: bold;">two different cursor positions</span>, one for undo and one for redo.  
For instance, imagine we removed the text &#8216;world&#8217;. Undo (i.e. insert &#8216;<span style="font-style: italic;">hello</span>&#8216;) should set the cursor position to the end of &#8216;hello&#8217;. Redo (i.e. remove &#8216;<span style="font-style: italic;">hello</span>&#8216;) should set the cursor position to the start of it.

Luckily a programmer does not have to set the undo/redo cursor positions and text selections manually. [undoStart()][4] is called the first time editStart() is called. The closing editEnd() finally calls [undoEnd()][5]. So undoStart() sets the undo cursor position and undo text selection, while undoEnd() sets the redo cursor position and redo text selection.

<span style="font-size: 130%;"><span style="font-weight: bold;">Group Merging</span></span>

The careful reader might have noticed KateUndoGroup::merge(). So merging of two groups is also supported. Whether text operations should be merged into an existing undo group can be controlled with KateDocument::setUndoDontMerge(). Pasting text for example set&#8217;s this flag.

<span style="font-size: 130%;"><span style="font-weight: bold;">Undo and Redo</span></span>

Every document in KatePart has two lists: An <span style="font-weight: bold;">undo list</span>, and a <span style="font-weight: bold;">redo list</span>. Suppose we have 10 KateUndoGroups in the undo list and the user invokes undo 4 times. Then the undo list only contains 6 items and the redo list 4. Now it is also possible to redo. However, typing text clears the redo list.

<span style="font-size: 130%;"><span style="font-weight: bold;">Document Modified Flag</span></span>

KateDocument::updateModifed() is called to update the <span style="font-weight: bold;">modified flag</span> of a file. This update also relies on the current active undo group. Saving the file saves a pointer to the current undo group, and later we simply can check whether the current undo group is the active one. Pretty simple mechanism.

 [1]: http://doc.trolltech.com/4.4/qundostack.html
 [2]: http://api.kde.org/4.x-api/kdelibs-apidocs/kate/html/katedocument_8cpp-source.html#l00933
 [3]: http://api.kde.org/4.x-api/kdelibs-apidocs/kate/html/katedocument_8cpp-source.html#l01043
 [4]: http://api.kde.org/4.x-api/kdelibs-apidocs/kate/html/katedocument_8cpp-source.html#l00964
 [5]: http://api.kde.org/4.x-api/kdelibs-apidocs/kate/html/katedocument_8cpp-source.html#l00976