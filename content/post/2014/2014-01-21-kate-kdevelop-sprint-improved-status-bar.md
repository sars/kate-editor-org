---
title: Kate/KDevelop Sprint – Improved Status Bar
author: Christoph Cullmann

date: 2014-01-21T17:50:43+00:00
url: /2014/01/21/kate-kdevelop-sprint-improved-status-bar/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
Just started to introduce a default status bar to KatePart.

Instead of  forcing all applications to implement an own one with varying quality and features, it provides a default status bar that is an improved variant of the status bar of the Kate application. Now Kate and KWrite have exactly the same features there ;)

If the host application doesn&#8217;t want that, the KTextEditor interface allows to deactivate it completely.

The new status bar allows e.g. finally to switch the file mode (== highlighting) and encoding directly from the status bar ;)

Mandatory screenshot:  
[<img class="size-full wp-image-3117 aligncenter" alt="KWrite KF5 with new status bar" src="/wp-content/uploads/2014/01/kwrite.png" width="623" height="392" srcset="/wp-content/uploads/2014/01/kwrite.png 623w, /wp-content/uploads/2014/01/kwrite-300x188.png 300w" sizes="(max-width: 623px) 100vw, 623px" />][1]

 [1]: /wp-content/uploads/2014/01/kwrite.png