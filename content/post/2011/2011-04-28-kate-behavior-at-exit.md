---
title: Kate Behavior at Exit
author: Dominik Haumann

date: 2011-04-28T20:37:16+00:00
url: /2011/04/28/kate-behavior-at-exit/
categories:
  - KDE
tags:
  - planet

---
From time to time it happens that bug reports get pretty much nasty. A single user or some users request a feature. If you argue, bashing starts.

In this case, it&#8217;s about Kate and the following scenario: You are editing several files (e.g. because you clicked on text files in dolphin). So Kate starts and you are not using an explicit session. If all files are saved, you can simply quit Kate. <a href="https://bugs.kde.org/show_bug.cgi?id=267356" target="_blank">Bug #267356 requests</a>, that the user is asked whether to save the session or not. Right now, if you are not using a session, Kate simply closes and the file list is lost. Comments are welcome, <a href="https://bugs.kde.org/show_bug.cgi?id=267356" target="_blank">especially in the report</a>. But _please_ be objective and polite. Thanks&#8230;

PS: Sometimes open source development is real fun. Sometimes it&#8217;s not :p