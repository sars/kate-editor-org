---
title: Kate Turning 10 Years Old
author: Dominik Haumann

date: 2011-08-11T14:59:39+00:00
url: /2011/08/11/kate-turning-10-years-old/
categories:
  - Common
  - KDE
  - Users
tags:
  - planet

---
<p style="text-align: left;">
  We almost missed it, but 10 years ago Kate was included in KDE&#8217;s CVS repository and shipped <a title="KDE 2.2 Announcement" href="http://www.kde.org/announcements/changelogs/changelog2_1to2_2.php" target="_blank">the first time with KDE 2.2</a>. Details about the history <a title="Kate History" href="/2010/08/15/kate-history/">can be found here</a>. So Happy Birthday Kate! And Kate in KDE 2.2 looked like this:
</p>

<p style="text-align: center;">
  <a href="/wp-content/uploads/2011/08/kate-in-kde2.2.png"><img class="aligncenter size-full wp-image-1165" title="Kate in KDE 2.2" src="/wp-content/uploads/2011/08/kate-in-kde2.2.png" alt="" width="644" height="482" srcset="/wp-content/uploads/2011/08/kate-in-kde2.2.png 1074w, /wp-content/uploads/2011/08/kate-in-kde2.2-300x224.png 300w, /wp-content/uploads/2011/08/kate-in-kde2.2-1024x766.png 1024w" sizes="(max-width: 644px) 100vw, 644px" /></a>
</p>

<p style="text-align: left;">
  In comparison, Kate nowadays looks like this (KDE 4.7):
</p>

<p style="text-align: center;">
  <a href="/wp-content/uploads/2011/08/kate-in-kde4.7.png"><img class="aligncenter size-full wp-image-1168" title="Kate in KDE 4.7" src="/wp-content/uploads/2011/08/kate-in-kde4.7.png" alt="" width="608" height="410" srcset="/wp-content/uploads/2011/08/kate-in-kde4.7.png 950w, /wp-content/uploads/2011/08/kate-in-kde4.7-300x202.png 300w" sizes="(max-width: 608px) 100vw, 608px" /></a>
</p>

<p style="text-align: center;">