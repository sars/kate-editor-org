---
title: One great man less :(
author: Christoph Cullmann

date: 2011-10-13T15:03:16+00:00
url: /2011/10/13/one-great-man-less/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - Users
tags:
  - planet

---
[Dennis MacAlistair Ritchie][1] died at home this weekend.

<figure id="attachment_1600" aria-describedby="caption-attachment-1600" style="width: 259px" class="wp-caption aligncenter">[<img src="/wp-content/uploads/2011/10/Dennis_MacAlistair_Ritchie_-259x300.jpg" alt="" title="Dennis MacAlistair Ritchie" width="259" height="300" class="size-medium wp-image-1600" srcset="/wp-content/uploads/2011/10/Dennis_MacAlistair_Ritchie_-259x300.jpg 259w, /wp-content/uploads/2011/10/Dennis_MacAlistair_Ritchie_.jpg 426w" sizes="(max-width: 259px) 100vw, 259px" />][2]<figcaption id="caption-attachment-1600" class="wp-caption-text">Dennis MacAlistair Ritchie</figcaption></figure>

As the creator of the [C programming language][3] and one of the main developers of [Unix][4], he impacted the life of me and other developers a lot.

Even today, 30 years after their initial creation, many people work on Unix like systems (like Linux or Mac OS) and develop in (Objective) C(++).

For me C was one of my starting languages for my real programming work and even today I analyze the whole day software written in C for embedded systems that control our modern world, be it the flight control of airplanes or engine control of cars.

Without his initial ideas and work, today there would be no Linux kernel, no Mac OS, &#8230;

We all owe him a lot. He changed the world.

Thanks for your great inventions! You won&#8217;t be forgotten.

Just two impressive Ritchie cites:

_&#8220;Unix is simple and coherent, but it takes a genius – or at any rate a programmer – to understand and appreciate the simplicity.&#8221;_

_&#8220;The greatest danger to good computer science research today may be excessive relevance. If we can keep alive enough openess to new ideas, enough freedom of communication, enough patience to allow the novel to prosper, it will remain possible for a future Ken Thompson to find a little-used Cray/1 computer and fashion a system as creative, and as influential, as Unix.&#8221;_

 [1]: http://en.wikipedia.org/wiki/Dennis_Ritchie
 [2]: /wp-content/uploads/2011/10/Dennis_MacAlistair_Ritchie_.jpg
 [3]: http://en.wikipedia.org/wiki/C_(programming_language)
 [4]: http://en.wikipedia.org/wiki/Unix