---
title: Kate in KDE 4.8
author: Dominik Haumann

date: 2011-12-20T23:17:07+00:00
url: /2011/12/21/kate-in-kde-4-8/
pw_single_layout:
  - "1"
categories:
  - KDE
  - Users
tags:
  - planet

---
About half a year ago, [Kate 3.7 was released][1] as part of KDE 4.7. Now that <a title="KDEE 4.8 Release Schedule" href="http://techbase.kde.org/Schedules/KDE4/4.8_Release_Schedule" target="_blank">KDE 4.8 is about to be released</a>, let&#8217;s have a look at what Kate brings in KDE 4.8.

#### Bug Fixes

According to KDE&#8217;s bug tracker, <a title="List of fixed issues in KDE 4.8" href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=kate&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&resolution=FIXED&resolution=WORKSFORME&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-08-01&chfieldto=2011-12-31&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=" target="_blank">about 190 issues were solved</a>. Taking a closer look, lots of small issues were fixed, amongst them:

  * improved printing support (<a title="Print Regions" href="https://bugs.kde.org/show_bug.cgi?id=111086" target="_blank">report 1</a>, <a title="Box Color wrong" href="https://bugs.kde.org/show_bug.cgi?id=200248" target="_blank">report 2</a>, <a title="Printing: Kate no longer knows %P" href="https://bugs.kde.org/show_bug.cgi?id=246699" target="_blank">report 3</a>, <a title="Dynamically wrapped lines not printed correctly" href="https://bugs.kde.org/show_bug.cgi?id=202229" target="_blank">report 4</a>, <a title="Print Margins" href="https://bugs.kde.org/show_bug.cgi?id=56077" target="_blank">report 5</a>, and more)
  * syntax highlighting files added and updated
  * <a title="Zoom with ctrl + mouse wheel" href="https://bugs.kde.org/show_bug.cgi?id=194922" target="_blank">zoom in/out with ctrl+mouse wheel</a>, <a title="Scroll over folding area" href="https://bugs.kde.org/show_bug.cgi?id=199464" target="_blank">scroll over folding area</a>
  * <a title="Wrong active view when resuming session with split view" href="https://bugs.kde.org/show_bug.cgi?id=195435" target="_blank">wrong active view when resuming session with split view</a>
  * <a title="Ambiguous shortcuts in File System Browser" href="https://bugs.kde.org/show_bug.cgi?id=236368" target="_blank">ambiguous shortcuts in file system browser resolved</a>
  * improved GDB plugin
  * we heavily <a title="Cleanup of the Kate Bug Database" href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=kate&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-08-01&chfieldto=2011-12-31&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=" target="_blank">cleaned up our bug database</a> (bugs + wishes):

<img class="alignnone size-full wp-image-1654" title="Kate Bug Charts" src="/wp-content/uploads/2011/12/kate-bugs.png" alt="" width="800" height="600" srcset="/wp-content/uploads/2011/12/kate-bugs.png 800w, /wp-content/uploads/2011/12/kate-bugs-300x225.png 300w" sizes="(max-width: 800px) 100vw, 800px" /> 

#### Heavily Reworked Code Folding

As part of the Google Summer of Code (GSoC) 2011, the code folding code was improved. The main goal was to eliminate wrong folding behavior and fix all crashes while making the code more maintainable. We are quite happy with the results: Given the robust behavior, it now was possible to remember code folding state past sessions and during document reload. So folded regions are not lost anymore.

Additionally, the visual appearance was changed to be rather decent.

#### Improved Vi Mode

Also as part of the GSoC 2011, Kate&#8217;s vi input mode was heavily improved.

#### Search & Replace in Files

The &#8220;Find in Files&#8221; plugin is now replaced with the new &#8220;Search and Replace&#8221; plugin: It basically contains all the features of &#8220;Find in Files,&#8221; but additionally has the following features:

  * either search in files-on-disk, or in all the opened documents
  * all search matches are highlighted
  * replace support: matches can be (selectively) replaced with other text

<img class="size-full wp-image-1650 alignnone" title="Search & Replace in Files" src="/wp-content/uploads/2011/12/search-replace.png" alt="" width="599" height="294" srcset="/wp-content/uploads/2011/12/search-replace.png 599w, /wp-content/uploads/2011/12/search-replace-300x147.png 300w" sizes="(max-width: 599px) 100vw, 599px" /> 

#### Line Modification Indicators

Kate has a shiny new line modification system. Read all about it in [this dedicated article][2]. Mandatory screenshot:

<img class="alignnone size-full wp-image-1652" title="Kate Line Modification Indicatiors" src="/wp-content/uploads/2011/12/line-modification-system.png" alt="" width="467" height="168" srcset="/wp-content/uploads/2011/12/line-modification-system.png 467w, /wp-content/uploads/2011/12/line-modification-system-300x107.png 300w" sizes="(max-width: 467px) 100vw, 467px" /> 

#### Document Variable (Modeline) Editor

Kate (and thus all applications use Kate Part) can be configured by using document variables, also known as modelines. Since it&#8217;s hard to remember all the available keys and values, a dialog helps out in the config dialog. You can read about all the details in [this dedicated post][3] (screenshots).

#### Documentation Updates

T.C. Hollingworth put a huge amount of work into the Kate and KWrite handbook. Thus, the documentation is much more up-to-date and we hope it helps to learn using Kate effectively more quickly. The official version is available on [docs.kde.org][4].

## Current State & The Road Ahead

Apart from the upcoming KDE 4.8 release, [Kate turned 10 years old][5] this summer. All in all, we can proudly say that Kate has a solid code base in its current state. The smart ranges were replaced by the moving ranges. Code folding is more clean with less bugs. Our plugins got a lot of updates. The documentation has improved. Of course, more help is always welcome. So if you are interested in Kate development, you are welcome to [build Kate from sources][6] and [join our team][7] by sending patches.

Thanks to everyone contributing to this great release :)

 [1]: /2011/07/09/kate-in-kde-4-7/ "Kate in KDE 4.7"
 [2]: /2011/09/06/line-modification-system/ "Kate Line Modification System"
 [3]: /2011/07/23/kate-modeline-editor/ "Kate Modeline Editor"
 [4]: http://docs.kde.org/development/en/kde-baseapps/kate/index.html "The Kate Handbook"
 [5]: /2011/08/11/kate-turning-10-years-old/ "Kate turned 10"
 [6]: /get-it/ "Building Kate"
 [7]: /join-us/ "Join Kate Development"