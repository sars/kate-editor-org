---
title: Going to the Desktop Summit 2011 in Berlin :)
author: Christoph Cullmann

date: 2011-06-25T10:28:14+00:00
url: /2011/06/25/going-to-the-desktop-summit-2011-in-berlin/
categories:
  - Common
  - Events
  - KDE
tags:
  - planet

---
<table>
  <tr>
    <td>
      Just a quick &#8220;me too&#8221; post, will be around in Berlin ;)</p> 
      
      <p>
        Hope to meet up with a lot of KDE people once more.<br /> For the Kate team, at least me, Dominik and Erlend are around there.</td> 
        
        <td>
          <img src="/wp-content/uploads/2011/06/DS2011banner.png" alt="Going to Desktop Summit 2011" />
        </td></tr> </table>