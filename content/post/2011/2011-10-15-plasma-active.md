---
title: Plasma Active
author: Dominik Haumann

date: 2011-10-15T18:09:20+00:00
url: /2011/10/15/plasma-active/
pw_single_layout:
  - "1"
categories:
  - KDE
tags:
  - planet

---
At the desktop summit, many contributors got a ExoPC from Intel, but the software on it [was quite a disappointment][1]. Meanwhile, there is <a title="Plasma Active One Unleashed" href="http://kde.org/announcements/plasma-active-one/" target="_blank">an official release</a> of <a title="Plasma Active" href="http://plasma-active.org/" target="_blank">Plasma Active</a> that fills the gap. So I sat down and installed it on the ExoPC. It really works quite nice and smooth. Applications like Amarok and a browser make it usable to hear music and do some quick internet surfing. I documented the steps in order to get everything up and running.

#### Running Plasma Active One from the USB Stick (LIVE version)

To get a quick impression, you can download the file <a title="open-slx" href="http://download.open-slx.com/iso/11.4/" target="_blank">plasma-active-one.iso from open-slx.com</a>. Then, plug in a USB stick to your computer and run the comand

<pre>sudo dd if=plasma-active-one.iso of=/dev/&lt;DEVICE&gt; bs=1M</pre>

where <DEVICE> corresponds to the USB stick (in my case, this is sdc). When finished, plug the USB stick into the upper USB port of your ExoPC and press the power on button. Make sure to tap &#8220;BBS&#8221; immediately and choose the USB stick as boot device. After this, choose the first entry to start the live version of Plasma Active.

Note: As of 2011-10-15, a installation with this live version is not possible (even though some documentation at some point or the splash screen suggest otherwise).

#### Installing Plasma Active on the Hard Disk

In order to get Plasma Active on your hard disk, you first have to install openSUSE 11.4. To this end,

  1. <a title="Download openSUSE 11.4" href="http://software.opensuse.org/114/en" target="_blank">download the 4.7 GB DVD image of openSUSE 11.4 (32 bit)</a>, and copy it on the USB stick (in my case <DEVICE> = sdc):  
    sudo dd if=openSUSE-11.4-DVD-i586.iso of=/dev/<DEVICE> bs=1M
  2. plug the USB stick into the upper USB port of your ExoPC and press the power on button
  3. make sure to tap &#8220;BBS&#8221; immediately and choose the USB stick as boot device
  4. after this, choose &#8220;Installation&#8221; to start the install process (you need an external keyboard on the lower USB port)
  5. follow the installation routine, choose the KDE desktop, do the partition setup, create a new user account and finally start the installation
  6. copying files takes some time. After this, the system finally boots to proceed with the automatic configuration.
  7. Finally, the system arrives in KDE. (From now on, the USB stick is not needed anymore.)

The touch screen does not work out of the box. Hence, I restarted X from the console with /etc/init.d/xdm restart. After that, the external mouse and keyboard worked (ignore the fact that you might be greeted by 6 crashed akonadi windows).

Once you have the internet up and running, proceed with <a title="Installing Plasma Active" href="http://community.kde.org/Plasma/Active/Installation#Installation_on_Balsam_Professional_or_openSUSE" target="_blank">1.5 Installation on Balsam Professional or openSUSE</a>. This updates your kernel to properly support the touchpad and replaces the default Plasma Desktop with Plasma Active.

Kudos to the Plasma Active team and all involved contributors and supporters!

PS: Although a text editor is probably not of much use on a tablet PC, KWrite is available :-)

 [1]: /2011/08/11/plasma-active-the-stage-is-yours/ "Pasma Active - The Stage is Yours"