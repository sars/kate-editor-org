---
title: Evolution of Kate Development
author: Dominik Haumann

date: 2011-11-03T21:49:44+00:00
url: /2011/11/03/evolution-of-kate-development/
pw_single_layout:
  - "1"
categories:
  - KDE
  - Users
tags:
  - planet

---
Recently, the [Kate project celebrated its 10th anniversary][1]. The gource visualization of 10 years Kate development (2001 to 2011) looks as follows. Have fun :-)



PS: In case you do not see any video, [click here][2].

PPS:Launch gource on kdelibs, that&#8217;s much more interesting!

 [1]: /2011/08/11/kate-turning-10-years-old/ "Kate turned 10"
 [2]: /2011/11/03/evolution-of-kate-development/ "Evolution of Kate Development"