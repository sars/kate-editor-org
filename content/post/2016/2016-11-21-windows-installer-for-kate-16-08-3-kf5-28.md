---
title: Windows installer for Kate 16.08.3 KF5.28
author: Kåre Särs

date: 2016-11-21T09:59:48+00:00
url: /2016/11/21/windows-installer-for-kate-16-08-3-kf5-28/
categories:
  - Common

---
New Kate installer for Windows!

Except for the general bug-fixes and feature additions in Kate and KF5 this installer also fixes a rename file failure bug by including the needed KIO dll.

[<img class="aligncenter size-full wp-image-3993" src="/wp-content/uploads/2016/11/Kate1.png" alt="Kate 16.08.3 KF5.28" width="704" height="615" srcset="/wp-content/uploads/2016/11/Kate1.png 704w, /wp-content/uploads/2016/11/Kate1-300x262.png 300w" sizes="(max-width: 704px) 100vw, 704px" />][1]

Grab it while it is hot: [Kate 16.08.3 KF5.28 64bit][2] or [Kate 16.08.3 KF5.28 32bit][3]

 [1]: /wp-content/uploads/2016/11/Kate1.png
 [2]: http://download.kde.org/stable/kate/Kate-setup-16.08.3-KF5.28-64bit.exe
 [3]: http://download.kde.org/stable/kate/Kate-setup-16.08.3-KF5.28-32bit.exe