---
title: First Day in Randa
author: Christoph Cullmann

date: 2016-06-13T06:41:02+00:00
url: /2016/06/13/first-day-in-randa/
categories:
  - Common
  - Developers
  - Events
tags:
  - planet

---
Yesterday afternoon I and others arrived in Randa. After some first meetup, started to try out how much work it is to e.g. compile Konsole or KDevelop on my Mac without patching Qt and stuff.

Two review requests pending to get Konsole working at least on some basic level. Lets see what more is needed to get it in some usable state (at least the terminal toolview in Kate now can use the konsole part ;=)

[<img class="alignnone" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][1]

 [1]: https://www.kde.org/fundraisers/randameetings2016/