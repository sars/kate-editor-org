---
title: 'Kate Scripting Updates & Zen-like Quick Coding'
author: Dominik Haumann

date: 2012-11-06T17:41:15+00:00
url: /2012/11/06/kate-scripting-updates-zen-like-quick-coding/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
In KDE >= 4.10, Kate Part&#8217;s scripting changed a bit. The changes are already documented in the <a title="Scripting in Kate" href="http://docs.kde.org/development/en/kde-baseapps/kate/advanced-editing-tools-scripting.html" target="_blank">Kate handbook</a>, but we&#8217;ll quickly have a look into it now anyway.

Prior to KDE SC 4.10, scripts in Kate Part always had to contain the &#8220;type&#8221; in the scripting header, like indentation or command. This was changed to simply using different folders (system wide or in your $HOME kde folder) as follows:

  * indenters are located in share/apps/katepart/script/indentation
  * commands are located in share/apps/katepart/script/commands
  * api is located in share/apps/katepart/script/libraries

Further, the API is not loaded automatically anymore. Instead, we introduced the function **require(&#8220;file.js&#8221;)**, which takes one argument. For instance, if you want to use the Cursor and Range API, you just write

> <pre>require("cursor.js");
require("range.js");</pre>

In this example, it would even suffice to just write require(&#8220;range.js&#8221;); since range.js itself contains the line require(&#8220;cursor.js&#8221;); Kate Part tracks, which files are already included through require, so you don&#8217;t have to worry about include guards like in C/C++.

Now as we are able to just load the libraries that we really need, we can add as many libraries as wanted. For instance, the author of zen-coding wrote a thin wrapper to make all the zen-coding goodies available to Kate Part. It&#8217;s included in the emmet subfolder in the script/libraries folder. So if you need this stuff, you just write require(&#8220;emmet/desired-file.js&#8221;);

It&#8217;s a bit unfortunate that we still break how the scripting works from time to time, but it&#8217;s better to fix and improve the scripting instead of living with limited capabilities. So if you have own scripts, it&#8217;s the best time to <a title="Kate developer mailing list" href="mailto:kwrite-devel@kde.org" target="_blank">contribute them to Kate</a>!

##### Generic Quick Coding Features

Further, we have some new <a title="Quick Coding Features" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/master/entry/part/script/data/commands/quickcoding.js" target="_blank">command line script called quickcoding.js</a> that already uses the zen-coding idea: If you for instance write

> <pre> c#n:Test#p:Parent</pre>

and press Ctrl+Alt+# in C++ files (the Mode must be C++!), it will automatically expand to

> <pre>/**
 * Class Test
 */
class Test : public Parent
{
  public:
    /**
     * Constructor of Test
     */
    Test ();

    /**
     * Destructor of Test
     */
    ~Test ();
};</pre>

So how does it work? Since the file mode is C++, the quick coding command will read files from <a title="Quick Coding File Location" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/master/show/part/script/data/files/quickcoding/cpp" target="_blank">katepart/script/files/quickcoding/cpp/</a>. The first character of this cryptic string is &#8216;c&#8217;, which tells the quick coding command to look into <a title="c.template quick coding file" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/master/entry/part/script/data/files/quickcoding/cpp/c.template" target="_blank">c.template</a>. This template now expands according to the specified arguments: n:Test sets the class name to Test, and the optional p:Parent derives Test from Parent.

This is just a proof of concept, and you can do the same with Text Snippts in Kate. But still, this is another way adding support for quick coding for arbitrary languges, and the implementation is completely in JavaScript, so it is rather easy to extend and customize it to your needs. We&#8217;ll probably extend/change this feature a bit more over time, let&#8217;s see :-) It&#8217;s also not yet documented in the Kate Handbook. We&#8217;ll probably add &#8216;official&#8217; documentation for this in later Kate releases. Happy coding!