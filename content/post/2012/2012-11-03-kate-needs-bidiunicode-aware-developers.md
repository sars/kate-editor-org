---
title: Kate needs BiDi/Unicode aware developers ;)
author: Christoph Cullmann

date: 2012-11-03T12:32:38+00:00
url: /2012/11/03/kate-needs-bidiunicode-aware-developers/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
tags:
  - planet

---
During our long bug hunting and fixing sprint I came along some nasty BiDi or Unicode related bugs.

I and most others in the Kate team are not really able to fix such issues :( We lack both the knowledge of the underlying techniques and are not able to write/read any language that really uses complex unicode chars or BiDi text.

Therefore: If you know Qt and you want to help out with fixing BiDi/Unicode related bugs, please provide patches for them in the KDE Bugzilla!

Here are the relevant bugs:

  * <https://bugs.kde.org/show_bug.cgi?id=70986>
  * <https://bugs.kde.org/show_bug.cgi?id=165397>
  * <https://bugs.kde.org/show_bug.cgi?id=172630>
  * <https://bugs.kde.org/show_bug.cgi?id=187408>
  * <https://bugs.kde.org/show_bug.cgi?id=192458>
  * <https://bugs.kde.org/show_bug.cgi?id=205447>
  * <https://bugs.kde.org/show_bug.cgi?id=237515>
  * <https://bugs.kde.org/show_bug.cgi?id=280645>

Any help here is highly appreciated! If you have questions about the internals of KatePart needed to fix such an issue, just ask in the bug or kwrite-devel@kde.org.