---
layout: index
---
Kate는 [KDE](https://kde.org) 2.2부터 포함된 다중 문서 편집기입니다. [KDE 프로그램](https://kde.org/applications)의 일부로 배포됩니다. Kate는 네트워크 투명성을 비롯한 KDE의 강력한 기능과 통합되어 있습니다. Konqueror의 HTML 소스 코드를 보거나, 설정 파일을 편집하거나, 새로운 프로그램을 개발하거나 다른 텍스트 편집 작업에 사용해 보세요. 이 모든 기능을 단 하나의 Kate 창에서 사용할 수 있습니다. [더 알아보기...](/about/)

![Kate에 여러 문서와 빌드용 터미널 에뮬레이터를 열어 둔 스크린샷](/images/kate-window.png)