---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
discoverData:
  kate:
    alt: Kate via Discover of andere AppStream-stores installeren
    subtitle: via Discover
    title: Kate installeren
  kwrite:
    alt: Write via Discover of andere AppStream-toepassingenstores installeren
    subtitle: via Discover
    title: KWrite installeren
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /sass/get-it.scss
title: Kate verkrijgen
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}} 

### Linux & Unices 

+ [Kate](https://apps.kde.org/en/kate) or [KWrite](https://apps.kde.org/en/kwrite) installeren uit [uw distributie](https://kde.org/distributions/)

{{< get-it-button-discover >}} 

Deze knoppen werken alleen met [Discover](https://apps.kde.org/en/discover) en andere AppStream toepassingen-stores, zoals [GNOME Software](https://wiki.gnome.org/Apps/Software). U kunt ook de pakketbeheerder van uw distributie gebruiken, zoals [Muon](https://apps.kde.org/en/muon) of [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}} 

+ [Snap pakket in Snapcraft van Kate](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-badge-en.png" width="200" alt=`Kate uit de Snap-store ophalen` />}}

+ [Uitgave van Kate (64bit) AppImage pakket](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [Nachhtelijk van Kate (64bit) AppImage pakket](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **
+ [Bouw het](/build-it/#linux) uit broncode.

{{< /get-it >}} 

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}} 

### Windows 

+ [Kate in de Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.png" width="200" alt=`Kate uit de Microsoft-store ophalen` />}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate) 
+ [Kate uitgave (64bit) installatieprogramma](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ [Kate nachtelijk (64bit) installatieprogramma](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Bouw het](/build-it/#windows) uit broncode.

{{< /get-it >}} 

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}} 

### macOS 

+ [Kate uitgave installatieprogramma](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate nachtelijk installatieprogramma](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Bouw het](/build-it/#mac) uit broncode.

{{< /get-it >}} 



{{< get-it-info >}} 

**Over de uitgaven:** <br>[Kate](https://apps.kde.org/en/kate) en [KWrite](https://apps.kde.org/en/kwrite) zijn onderdeel van [KDE Applications](https://apps.kde.org), die [typisch 3 keer per jaar en-masse](https://community.kde.org/Schedules) worden uitgegeven. De engines [tekstbewerking](https://api.kde.org/frameworks/ktexteditor/html/) en de [syntaxisaccentuering](https://api.kde.org/frameworks/syntax-highlighting/html/) worden geleverd door [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), die [maandelijks worden bijgewerkt](https://community.kde.org/Schedules/Frameworks). Nieuwe uitgaven worden [hier](https://kde.org/announcements/) aangekondigd.

\* De **uitgave** pakketten bevatten de laatste versie van Kate en KDE Frameworks.

\*\* De **nachtelijke** pakketten worden automatisch dagelijks gecompileerd uit broncode, ze kunnen daarom instabiel zijn en bugs of onvolledige functies bevatten. Deze worden alleen aanbevolen voor testdoelen.

{{< /get-it-info >}} 