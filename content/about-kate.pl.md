---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: Możliwości
---
## Możliwości programu

![Zrzut ekranu pokazujący możliwość podziału okna Kate oraz wtyczka terminalu](/images/kate-window.png)

+ View and edit multiple documents at the same time, by splitting horizontally and vertically the window 
+ Lot of plugins: [Embedded terminal](https://konsole.kde.org), SQL plugin, build plugin, GDB plugin, Replace in Files, and more
+ Multi-document interface (MDI)
+ Session support

## Ogólne możliwości

![Zrzut ekranu pokazujący możliwość szukania i zastępowania Kate](/images/kate-search-replace.png)

+ Encoding support (Unicode and lots of others) 
+ Bi-directional text rendering support
+ Line ending support (Windows, Unix, Mac), including auto detection
+ Network transparency (open remote files)
+ Extensible through scripting

## Rozszerzone możliwości edytora

![Zrzut ekranu Kate pokazujący ramkę z numerami wierszy i zakładkami](/images/kate-border.png)

+ Bookmarking system (also supported: break points etc.) 
+ Scroll bar marks
+ Line modification indicators
+ Line numbers
+ Code folding

## Podświetlanie składni

![Zrzut ekranu Kate pokazujący możliwości podświetlania składni](/images/kate-syntax.png)

+ Highlighting support for over 300 languages 
+ Bracket matching
+ Smart on-the-fly spell checking
+ Highlighting of selected words

## Możliwości programowania

![Zrzut ekranu Kate pokazujący możliwości programowania](/images/kate-programming.png)

+ Scriptable auto indentation 
+ Smart comment and uncomment handling
+ Auto completion with argument hints
+ Vi input mode
+ Rectangular block selection mode

## Znajdź i zamień

![Zrzut ekranu Kate pokazujący możliwości przyrostowego wyszukiwania](/images/kate-search.png)

+ Incremental search, also known as &#8220;find as you type&#8221; 
+ Support for multiline search & replace
+ Regular expression support
+ Search & replace in multiple opened files or files on disk

## Kopia zapasowa i przywracanie

![Zrzut ekranu Kate pokazujący możliwości powrotu z usterki](/images/kate-crash.png)

+ Backups on save 
+ Swap files to recover data on system crash
+ Undo / redo system