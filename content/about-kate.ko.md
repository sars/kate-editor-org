---
author: Christoph Cullmann
date: 2010-07-09 08:40:19+00:00
hideMeta: true
menu:
  main:
    weight: 2
title: 기능
---
## 프로그램 기능

![Kate 창 나누기 기능과 터미널 플러그인 스크린샷](/images/kate-window.png)

+ View and edit multiple documents at the same time, by splitting horizontally and vertically the window 
+ Lot of plugins: [Embedded terminal](https://konsole.kde.org), SQL plugin, build plugin, GDB plugin, Replace in Files, and more
+ Multi-document interface (MDI)
+ Session support

## 일반 기능

![Kate 찾아 바꾸기 기능 스크린샷](/images/kate-search-replace.png)

+ Encoding support (Unicode and lots of others) 
+ Bi-directional text rendering support
+ Line ending support (Windows, Unix, Mac), including auto detection
+ Network transparency (open remote files)
+ Extensible through scripting

## 고급 편집기 기능

![줄 번호와 책갈피를 표시하는 Kate 창 모서리 부분 스크린샷](/images/kate-border.png)

+ Bookmarking system (also supported: break points etc.) 
+ Scroll bar marks
+ Line modification indicators
+ Line numbers
+ Code folding

## 구문 강조

![Kate 구문 강조 기능 스크린샷](/images/kate-syntax.png)

+ Highlighting support for over 300 languages 
+ Bracket matching
+ Smart on-the-fly spell checking
+ Highlighting of selected words

## 프로그래밍 기능

![Kate 프로그래밍 기능 스크린샷](/images/kate-programming.png)

+ Scriptable auto indentation 
+ Smart comment and uncomment handling
+ Auto completion with argument hints
+ Vi input mode
+ Rectangular block selection mode

## 찾아 바꾸기

![Kate 증분 검색 스크린샷](/images/kate-search.png)

+ Incremental search, also known as &#8220;find as you type&#8221; 
+ Support for multiline search & replace
+ Regular expression support
+ Search & replace in multiple opened files or files on disk

## 백업과 복원

![Kate 충돌 복구 스크린샷](/images/kate-crash.png)

+ Backups on save 
+ Swap files to recover data on system crash
+ Undo / redo system